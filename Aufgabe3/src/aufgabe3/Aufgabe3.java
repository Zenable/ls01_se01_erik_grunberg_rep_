package aufgabe3;

public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.printf("%-12s|%10s", "Fahrenheit", "Celcius");
		System.out.printf("%n%23s", "-----------------------");
		System.out.printf("%n%-12d|%10.2f", -20, -28.88889);
		System.out.printf("%n%-12d|%10.2f", -10, -23.33333);
		System.out.printf("%n%+-12d|%10.2f", 0, -17.77778);
		System.out.printf("%n%+-12d|%10.2f", 20, -6.66667);
		System.out.printf("%n%+-12d|%10.2f", 30, -1.11111);

	}

}
