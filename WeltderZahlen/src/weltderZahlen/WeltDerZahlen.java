package weltderZahlen;
/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
	  byte anzahlPlaneten =  9;
    
    // Anzahl der Sterne in unserer Milchstraße
       long anzahlSterne = 100000000000L;
    
    // Wie viele Einwohner hat Berlin?
       int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 7120;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       double gewichtKilogramm = 55.5; 
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17098242;
    
    // Wie groß ist das kleinste Land der Erde?
    
       double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Anzahl der Bewohner in Berlin: " + bewohnerBerlin);
    System.out.println("Alter in Tagen: " + alterTage);
    System.out.println("Gewicht in Kilo: " + gewichtKilogramm);
    System.out.println("Fl�che des gr��ten Landes in km²: " + flaecheGroessteLand);
    System.out.println("Fl�che des kleinsten Landes in km²: " + flaecheKleinsteLand);
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}

