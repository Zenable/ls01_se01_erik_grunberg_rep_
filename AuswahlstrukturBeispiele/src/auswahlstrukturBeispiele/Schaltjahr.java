package auswahlstrukturBeispiele;

import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Bitte geben Sie ein Jahr ein: ");
		int year = scan.nextInt();
		System.out.println(schalti(year));
		
	}
	
	public static boolean schalti(int year) {
		if(year <= -45) {
			System.out.println("Schaltjahre wurden erst nach 45 v.Chr. vom geilen Caeser der Stecher erfunden.");
		} else {
			return rule3(year);
		}
		return false;
	}
	
	public static boolean rule1(int year) {
		if(year % 4 == 0) {
			return true;
		}
		return false;
	}
	
	public static boolean rule2(int year) {
		if(year % 100 == 0) {
			return false;
		}
		return true;
	}
	
	public static boolean rule3(int year) {
		
		if(year <= 1582) {
			System.out.print("Nach alter Regel: ");
			return rule1(year);
		} else {
			if(year % 400 == 0) {
				return true;
			} else {
				if(rule1(year) == true) {
					System.out.print("Nach neuer Regel: ");
					if(rule2(year) == true) {
						return true;
					} else {
						return false;
					}
				} 
			}
		}
		return false;
	}
}
