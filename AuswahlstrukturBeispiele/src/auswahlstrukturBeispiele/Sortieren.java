package auswahlstrukturBeispiele;

import java.util.*;

public class Sortieren {
	
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		sortieren();

	}
	
	
	public static void sortieren() {
		
		System.out.print("1: ");
		char a = scan.next().charAt(0);
		System.out.print("\n2: ");
		char b = scan.next().charAt(0);
		System.out.print("\n3: ");
		char c = scan.next().charAt(0);
		
		if (a < b && a < c) {
			if(b < c) {
				System.out.println(a + "; " + b + "; " + c);
			} else {
				System.out.println(a + "; " + c + "; " + b);
			}
		} else if (b < a && b < c) {
			if(a < c) {
				System.out.println(b + "; " + a + "; " + c);
			} else {
				System.out.println(b + "; " + c + "; " + a);
			}
		} else {
			if(a < b) {
				System.out.println(c + "; " + a + "; " + b);
			} else {
				System.out.println(c + "; " + b + "; " + a);
			}
		}
	}

}
