import java.util.Scanner;

public class PCHaendler {
	
	static Scanner myScanner = new Scanner(System.in);

	public static void main(String[] args) {

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?");

		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		
		double preis = liesDouble("Geben Sie den Nettopreis ein:");


		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");


		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtBruttogepreis(nettogesamtpreis, mwst);
		
		/*double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);*/

		// Ausgeben
		
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		
		/*System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");*/

	}
	
	public static String liesString(String text) {
		System.out.println(text);
		return myScanner.next();
	}
	
	public static int liesInt(String text) {
		System.out.println(text);
		return myScanner.nextInt();
	}
	
	public static double liesDouble(String text) {
		System.out.println(text);
		return myScanner.nextDouble();
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtBruttogepreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
