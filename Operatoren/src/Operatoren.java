﻿/* Operatoren.java
   Uebung zu Operatoren in Java
   @author
   @version
*/
public class Operatoren {
  public static void main(String [] args){
    /* 1. Vereinbaren Sie zwei Ganzzahlen.*/
	  int z1, z2;
    System.out.println("UEBUNG ZU OPERATOREN IN JAVA\n");
    /* 2. Weisen Sie den Ganzzahlen die Werte 75 und 23 zu
          und geben Sie sie auf dem Bildschirm aus. */
    
    System.out.println("Zahl 1: " + (z1 = 75));
    System.out.println("Zahl 2: " + (z2 = 23));
    /* 3. Addieren Sie die Ganzzahlen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    System.out.println("Zahl 1 + Zahl 2 = " + (z1 + z2));
    /* 4. Wenden Sie alle anderen arithmetischen Operatoren auf die
          Ganzzahlen an und geben Sie das Ergebnis jeweils auf dem
          Bildschirm aus. */
    System.out.println("Zahl 1 - Zahl 2 = " + (z1 - z2));
    System.out.println("Zahl 1 * Zahl 2 = " + (z1 * z2));
    System.out.println("Zahl 1 / Zahl 2 = " + (z1 / z2));
    System.out.println("Zahl 1 % Zahl 2 = " + (z1 % z2));
    /* 5. Ueberprüfen Sie, ob die beiden Ganzzahlen gleich sind
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    if(z1 == z2) {
    	System.out.println(true);
    } else {
    	System.out.println(false);
    }
    /* 6. Wenden Sie drei anderen Vergleichsoperatoren auf die Ganzzahlen an
          und geben Sie das Ergebnis jeweils auf dem Bildschirm aus. */
    if(z1 <= z2) {
    	System.out.println("Zahl 2 ist größer");
    } else if(z1 >= z2) {
    	System.out.println("Zahl 1 ist größer");
    } else if(z1 != z2) {
    	System.out.println("Zahlen sind ungleich");
    }
    /* 7. Ueberprüfen Sie, ob die beiden Ganzzahlen im  Interval [0;50] liegen
          und geben Sie das Ergebnis auf dem Bildschirm aus. */
    for(int i = 0; i <= 50; i++) {
    	if(i == z1)
    		System.out.println("Zahl 1 liegt im Interval [0;50]");
    	
    	if(i == z2)
    		System.out.println("Zahl 2 liegt im Interval [0;50]");
    }
  }//main
}// Operatoren
