package raumschiff;

public class RaumschiffTest {

	public static void main(String[] args) {
		/*
		 * Raumschiffe initialisieren
		 * 
		 * Raumschiff Klingonen initialisieren und mit Ladung bestücken
		 */
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		klingonen.addLadung(l1);
		klingonen.addLadung(l2);
		
		/*
		 * Raumschiff Romulaner initialisieren und mit Ladung bestücken
		 */
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		Ladung l4 = new Ladung("Rote Materie", 2);
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(l3);
		romulaner.addLadung(l4);
		romulaner.addLadung(l5);
		
		/*
		 * Raumschiff Vulkanier initialisieren und mit Ladung bestücken
		 */
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		Ladung l6 = new Ladung("Forschungssonde", 35);
		Ladung l7 = new Ladung("Photonentorpedos", 3);
		vulkanier.addLadung(l6);
		vulkanier.addLadung(l7);
		
		/*
		 * Auszuführende Methoden laut Aufgabe
		 */
		
		klingonen.photonentorpedoSchiessen(romulaner); 				// Klingonen schießen mit einem Photonentorpedo auf Romulaner
		romulaner.phaserkanoneSchiessen(klingonen);					// Romulaner schießen mit der Phaserkanone auf Klingonen
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");		// Vulkanier geben eine Nachricht an alle
		klingonen.zustandRaumschiff();								// Klingonen geben ihren Schiffszustand aus 
		klingonen.ladungsverzeichnisAusgeben();						// Klingonen geben ihr Ladungsverzeichnis aus
		vulkanier.reparaturDurchführen(true, true, true, 5);		// Vulkanier reparieren ihr Schiff
		vulkanier.photonentorpedosLaden(3);							// Vulkanier laden ihre Photonentorpedos
		vulkanier.ladungsverzeichnisAufraeumen();					// Vulkanier räumen ihr Lager auf
		klingonen.photonentorpedoSchiessen(romulaner);				// Klingonen schießen mit Photontorpedos auf Romulaner
		klingonen.photonentorpedoSchiessen(romulaner);				// -- "" --
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
		for (int i = 0; i < klingonen.eintraegeLogbuchZurueckgeben().size(); i++) {
			System.out.println(klingonen.eintraegeLogbuchZurueckgeben().get(i));
		}
		
		/*Raumschiff r1 = new Raumschiff(3, 83, 43, 36, 4, 33, "SS SchönScheiß");
		Raumschiff r2 = new Raumschiff(0, 33, 100, 100, 100, 4, "BB BöserBernd"); 
		
		System.out.println(r1.getSchildeInProzent() + " Schilde");
		System.out.println(r1.getEnergieversorgungInProzent() + " Energie");
		System.out.println(r1.getHuelleInProzent() + " huelle");
		
		r1.reparaturDurchführen(false, true, false, 6);
		
		System.out.println(r1.getSchildeInProzent() + " Schilde");
		System.out.println(r1.getEnergieversorgungInProzent() + " Energie");
		System.out.println(r1.getHuelleInProzent() + " huelle");
		System.out.println("------------------------------------");
		
		System.out.println("Ladungsmenge");
		r1.addLadung(new Ladung("Photontorpedos", 3));
		r1.photonentorpedosLaden(3);
		System.out.println(r1.getLadungsverzeichnis().get(0).getMenge());
		System.out.println("------------------------------------");
		
		r1.photonentorpedoSchiessen(r2);
		r1.photonentorpedoSchiessen(r2);
		r1.phaserkanoneSchiessen(r2);
				
		r1.addLadung(new Ladung("Brot", 5));
		r1.addLadung(new Ladung("Salami", 2));
		r1.addLadung(new Ladung("Butter", 1));
		
		System.out.println("Ladungsverzeichnis vor Löschung");
		r1.ladungsverzeichnisAusgeben();
		System.out.println("Ladungsverzeichnis nach Löschung");
		r1.ladungsverzeichnisAufraeumen();
		r1.ladungsverzeichnisAusgeben();
		System.out.println("------------------------------------");
		System.out.println("Logbuch");
		
		for(int i = 0; i < r1.eintraegeLogbuchZurueckgeben().size(); i++) {
			System.out.println(r1.eintraegeLogbuchZurueckgeben().get(i));
    	}
		
		System.out.println(r1);*/
	}

}
