package raumschiff;

import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	
	private int photontorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	public Raumschiff() {
		this.photontorpedoAnzahl = 0;
		this.energieversorgungInProzent = 100;
		this.schildeInProzent = 100;
		this.huelleInProzent = 100;
		this.lebenserhaltungssystemeInProzent = 100;
		this.androidenAnzahl = 0;
		this.schiffsname = "Unknown";
		broadcastKommunikator.clear();
		this.ladungsverzeichnis.clear();
	}
	
	public Raumschiff(int photontorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photontorpedoAnzahl = photontorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		
		this.ladungsverzeichnis.add(neueLadung);
		
	}
	
	public void ladungsverzeichnisAusgeben() {
		
			System.out.println("Ladungsverzeichnis der " + schiffsname + ":");
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.print("\tBezeichnung: " + this.ladungsverzeichnis.get(i).getBezeichnung());
			System.out.println("\tMenge: " + this.ladungsverzeichnis.get(i).getMenge());
		}
		
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		
		if(this.photontorpedoAnzahl == 0) {
			nachrichtAnAlle(this.schiffsname + ":\t-=*Click*=-");
		} else {
			nachrichtAnAlle(this.schiffsname + ":\tPhotontorpedo abgeschossen");
			this.setPhotontorpedoAnzahl(this.photontorpedoAnzahl - 1);
			treffer(r);
		}
	}

	public void phaserkanoneSchiessen(Raumschiff r) {
		
		if(this.energieversorgungInProzent < 50) {
			nachrichtAnAlle(this.schiffsname + ":\t-=*Click*=-");
		} else {
			nachrichtAnAlle(this.schiffsname + ":\tPhaserkanone abgeschossen");
			setEnergieversorgungInProzent(energieversorgungInProzent - 50);
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {
		nachrichtAnAlle(r.schiffsname + " wurde getroffen!");
		
		if(r.getSchildeInProzent() > 50) {
			
			r.setSchildeInProzent(r.getSchildeInProzent() - 50);
			
		} else {
			
			if(r.getHuelleInProzent() < 0 || r.getHuelleInProzent() <= 50) {
				r.setHuelleInProzent(0);
			} else {
				r.setHuelleInProzent(r.getHuelleInProzent() - 50);
			}
				
			if(r.getEnergieversorgungInProzent() < 0 || r.getEnergieversorgungInProzent() <= 50) {
				r.setEnergieversorgungInProzent(0);
			} else {
				r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
			}
		
		}
		
		if(r.getHuelleInProzent() == 0) {
			nachrichtAnAlle(r.getSchiffsname() + ":\tMeine Lebenserhaltungssysteme wurden vollständig zerstört!");
		}
	}
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}
	
	public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
		int pt = 0;
		int zaehl = 0;
		for (int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			if(this.ladungsverzeichnis.get(i).getBezeichnung().equals("Photontorpedos")) {
				pt += this.ladungsverzeichnis.get(i).getMenge();
				zaehl += i;
			}	
		}
		
		if(pt == 0) {
			System.out.println("Keine Photontorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		} else if(anzahlTorpedos > pt) {
			
			this.ladungsverzeichnis.get(zaehl).setMenge(this.ladungsverzeichnis.get(zaehl).getMenge() - pt);
			this.photontorpedoAnzahl += this.ladungsverzeichnis.get(zaehl).getMenge();
						
		}  else {
			
			this.ladungsverzeichnis.get(zaehl).setMenge(this.ladungsverzeichnis.get(zaehl).getMenge() - anzahlTorpedos);
			this.photontorpedoAnzahl += anzahlTorpedos;
			
		}
		
	}
	
	public void ladungsverzeichnisAufraeumen() {
		
		for(int i = 0; i < this.ladungsverzeichnis.size(); i++) {
			if(this.ladungsverzeichnis.get(i).getMenge() <= 0)
				this.ladungsverzeichnis.remove(i);
		}
		
	}
	
	public void reparaturDurchführen(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		Random rand = new Random();
		int zahl = rand.nextInt(101);
		Boolean[] reparieren = new Boolean[3];
		reparieren[0] = schutzschilde;
		reparieren[1] = energieversorgung;
		reparieren[2] = schiffshuelle;
		
		int anzhlRep = 0;
		
		for (int i = 0; i < reparieren.length; i++) {
			if(reparieren[i] == true)
				anzhlRep++;
		}
		
		if(anzahlDroiden > this.androidenAnzahl)
			anzahlDroiden = this.androidenAnzahl;
		if(anzhlRep == 0)
			return;
		
		int rep = (zahl * Math.abs(anzahlDroiden)) / Math.abs(anzhlRep);
		
		if(schutzschilde)
			setSchildeInProzent(this.schildeInProzent + rep);
		
		if(energieversorgung)
			setEnergieversorgungInProzent(this.energieversorgungInProzent + rep);
		
		if(schiffshuelle)
			setHuelleInProzent(this.huelleInProzent + rep);
	
	}
	
	public int getPhotontorpedoAnzahl() {
		return photontorpedoAnzahl;
	}

	public void setPhotontorpedoAnzahl(int photontorpedoAnzahl) {
		this.photontorpedoAnzahl = photontorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		if(energieversorgungInProzent > 100)
			energieversorgungInProzent = 100;
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		if(schildeInProzent > 100)
			schildeInProzent = 100;
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		if(huelleInProzent > 100)
			huelleInProzent = 100;
		
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
		
	public ArrayList<Ladung> getLadungsverzeichnis(){
		return ladungsverzeichnis;
	}
	
	public void zustandRaumschiff() {
		System.out.println("Zustand der " + this.getSchiffsname() + ":");
		System.out.println("Photontorpedoanzahl: " + photontorpedoAnzahl + "\nEnergieversorgung: "
				+ energieversorgungInProzent + "% \nSchilde: " + schildeInProzent + "% \nHuelle: "
				+ huelleInProzent + "% \nLebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent
				+ "% \nAndroidenanzahl: " + androidenAnzahl + " \nschiffsname:" + schiffsname);
	}
	
}
